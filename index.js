/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function userDetails() {
		console.log('Hello, ' + prompt('Enter your full name:') + '.');
		console.log('You are ' + prompt('Enter your age:') + ' years old.');
		console.log('You live in ' + prompt('Enter your current location:') + '.');
	};

	userDetails();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function favoriteBands(){
		console.log('1. Panic! at the Disco');
		console.log('2. Paramore');
		console.log('3. Arctic Monkeys');
		console.log('4. Imagine Dragons');
		console.log('5. IDKHBTFM');
	};

	favoriteBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function favoriteMovies(){
		console.log('1. Avengers: Endgame (2019)');
		console.log('Rotten Tomatoes Rating: 94%');
		console.log('2. Avengers: Infinity War (2018)');
		console.log('Rotten Tomatoes Rating: 85%')
		console.log('3. The Martian (2015)');
		console.log('Rotten Tomatoes Rating: 91%')
		console.log('4. The Platform (2020)');
		console.log('Rotten Tomatoes Rating: 79%')
		console.log('5. Dead Poets Society (1989)');
		console.log('Rotten Tomatoes Rating: 83%');
	};

	favoriteMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();
let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();

// console.log(friend1);
// console.log(friend2);